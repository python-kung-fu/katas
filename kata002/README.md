### Enunciado

Hay N ciudades. Para viajar desde la ciudad *i* a la ciudad *j* tardas `T[i][j]`.

Entre todos esos recorridos que empiezan en la ciudad 1, visitan todas las otras ciudades una sola vez, y vuelven a la ciudad 1 ¿Cuántas de esas rutas tardas exactamente K en recorrer?
