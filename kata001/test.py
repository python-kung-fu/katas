import os
import unittest
import timeit
import tracemalloc

repeticiones=100000
kata='kata_001'
funcion='canPlaceFlowers'
descripcion='''Supongamos que tienes un largo parterre en el que algunas de las parcelas 
están plantadas y otras no. 
Sin embargo, las flores no pueden ser plantadas en parcelas adyacentes - 
competirían por el agua y ambas morirían.
Dado un parterre (representado como una matriz que contiene 0 y 1, donde 0 
significa vacío y 1 significa no vacío), y un número n, regrese si se pueden 
plantar n nuevas flores en él sin violar la regla de no-adjuntar flores.'''

archivos = [ archivo for archivo in os.listdir() if archivo.startswith('kata001')]
usuarios=[usuario[8:-3] for usuario in archivos]
librerias=[usuario[:-3] for usuario in archivos]

datos=[]
for i in range (len(archivos)):
    datos.append([usuarios[i], librerias[i], archivos[i]])

class PruebasFuncion(unittest.TestCase):
    def test1(self):
        parametros=[
        ('prueba1',[1,0,0,0,1],1, True, ''), 
        ('prueba2',[1,0,0,0,1],2, False, ''), 
        ('prueba3',[0,0,1,0,0,0,0,1,0,0,1],2, True, ''), 
        ('prueba4',[0,0,1,0,0,0,0,1,0,0,1],3, False, ''), 
        ('prueba5',[1,1,0,0,0,0,1,0,0,0,0,0,1,0],3, True, ''), 
        ('prueba6',[1,1,0,0,0,0,1,0,0,0,0,0,1,0],4, False, '')]
        for p1, p2,  p3,  p4 , p5 in parametros:
            with self.subTest():
                self.assertEqual(fun(p2, p3), p4, f'{p1}---{p5}')

for dato in datos:
    exec(f"from {dato[1]} import {funcion} as fun")
    #Comprobación de errores
    resultado=unittest.main(verbosity=2, exit=False)
    errores=len(resultado.result.failures)+len(resultado.result.errors)
    dato.append(errores)
    tracemalloc.clear_traces()
    tracemalloc.start()
    #Tiempo de ejecución de la funcion a testear
    t1 = timeit.timeit(f"{funcion}([1,1,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,1,0,0],3)", f"from {dato[1]} import {funcion}", number=repeticiones) 
    t2 = timeit.timeit(f"{funcion}([1,1,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,1,0,0],7)", f"from {dato[1]} import {funcion}", number=repeticiones)
    t=t1+t2
    dato.append(t)
    current, peak = tracemalloc.get_traced_memory()
    dato.append(current/1000)
    dato.append(peak/1000)
    tracemalloc.stop()

    dato.append(len(open(dato[2]).read()))
#    tracemalloc.reset_peak()

    # Resultados 
print(f'{"KATA 001 PLANTAR FLORES":^80s}')
print(f'{"="*80}')
print(descripcion)
print(f'{"="*80}')
print('    Usuario                         Errores     Tiempo(s)    Memoria (kbs)   Meen. Pico (kbs)       Bytes')
for dato in datos:
    print(f'{dato[0]:<25}\t{dato[3]:8}{dato[4]:15.4f}{dato[5]:15.3f}{dato[6]:19.3f}{dato[7]:15}')
    

    
    
